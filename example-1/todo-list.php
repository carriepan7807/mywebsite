<?php
require_once("include/config.php");
$pageId="vue";
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <title>To Do List</title>
  </head>
  <body>  
    <?php include_once("header.php"); ?>
    <div class="container" id="app">
        <div class="page-header"></div>
        <div class="panel panel-info">
            <table class="table">
                <thead>
                    <tr>
                      <th scope="col" colspan="2">To do list </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <input type="text" class="form-control" placeholder="請輸入" v-model="message">
                        </td>
                        <td>
                            <button type="button" class="btn btn-primary" @click="makeList">OK</button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <ul class="list-group" v-for="(list, number) in list_ary">
              <li class="list-group-item list-group-item-action list-group-item-primary">
              {{ list }}
              <button type="button" class="btn btn-default" @click="deleteList(number)">
                <span class="glyphicon glyphicon-trash"></span>
              </button>
              </li>
            </ul>
        </div>
    </div>
    <script>   
    var app = new Vue({
      el: '#app',
      data: {
        message: '',
        seen: true,
        list_ary: ['買菜', '散步']
      },
      methods: {
        makeList: function() {
            if(this.message != '')
            {
                this.list_ary.push(this.message);
                this.message = '';
            }
          
        },
        deleteList: function(number)
        {
            this.list_ary.splice(number, 1);
        }
      }
    })
    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
